<?php

namespace Kindling\Customizer;

class Input
{
    protected $section;
    protected $optionName;
    protected $wpCustomize;

    public function __construct($wp_customize, string $section, string $optionName = 'kindling_customizer_options')
    {
        $this->wpCustomize = $wp_customize;
        $this->section = $section;
        $this->optionName = $optionName;
    }

    public function text(string $id, string $label, array $configuration = [])
    {
        $control = $configuration['control'] ?? [];
        $settings = $configuration['settings'] ?? [];

        return $this->base($id, $label, $configuration = [
            'control' => $control,
            'settings' => $settings,
        ]);
    }

    public function email(string $id, string $label, array $configuration = [])
    {
        $control = $configuration['control'] ?? [];
        $settings = $configuration['settings'] ?? [];

        $control['type'] = 'email';

        return $this->base($id, $label, $configuration = [
            'control' => $control,
            'settings' => $settings,
        ]);
    }

    public function url(string $id, string $label, array $configuration = [])
    {
        $control = $configuration['control'] ?? [];
        $settings = $configuration['settings'] ?? [];

        $control['type'] = 'url';

        return $this->base($id, $label, $configuration = [
            'control' => $control,
            'settings' => $settings,
        ]);
    }

    public function number(string $id, string $label, array $configuration = [])
    {
        $control = $configuration['control'] ?? [];
        $settings = $configuration['settings'] ?? [];

        $control['type'] = 'number';

        return $this->base($id, $label, $configuration = [
            'control' => $control,
            'settings' => $settings,
        ]);
    }

    public function hidden(string $id, string $label, array $configuration = [])
    {
        $control = $configuration['control'] ?? [];
        $settings = $configuration['settings'] ?? [];

        $control['type'] = 'hidden';

        return $this->base($id, $label, $configuration = [
            'control' => $control,
            'settings' => $settings,
        ]);
    }

    public function date(string $id, string $label, array $configuration = [])
    {
        $control = $configuration['control'] ?? [];
        $settings = $configuration['settings'] ?? [];

        $control['type'] = 'date';

        return $this->base($id, $label, $configuration = [
            'control' => $control,
            'settings' => $settings,
        ]);
    }

    public function radio(string $id, string $label, array $choices, string $default = null, array $configuration = [])
    {
        $control = $configuration['control'] ?? [];
        $settings = $configuration['settings'] ?? [];

        $control['choices'] = $choices;
        $control['type'] = 'radio';

        if (is_null($default)) {
            $default = key($choices);
        }

        $settings['default'] = $default;

        return $this->base($id, $label, $configuration = [
            'control' => $control,
            'settings' => $settings,
        ]);
    }

    public function select(string $id, string $label, array $choices, string $default = null, array $configuration = [])
    {
        $control = $configuration['control'] ?? [];
        $settings = $configuration['settings'] ?? [];

        $control['choices'] = $choices;
        $control['type'] = 'select';

        if (is_null($default)) {
            $default = key($choices);
        }

        $settings['default'] = $default;

        return $this->base($id, $label, $configuration = [
            'control' => $control,
            'settings' => $settings,
        ]);
    }

    public function checkbox(string $id, string $label, array $configuration = [])
    {
        $control = $configuration['control'] ?? [];
        $settings = $configuration['settings'] ?? [];

        $control['type'] = 'checkbox';

        return $this->base($id, $label, $configuration = [
            'control' => $control,
            'settings' => $settings,
        ]);
    }

    public function textarea(string $id, string $label, array $configuration = [])
    {
        $control = $configuration['control'] ?? [];
        $settings = $configuration['settings'] ?? [];

        $control['type'] = 'textarea';

        return $this->base($id, $label, $configuration = [
            'control' => $control,
            'settings' => $settings,
        ]);
    }

    public function pageDropdown(string $id, string $label, array $configuration = [])
    {
        $control = $configuration['control'] ?? [];
        $settings = $configuration['settings'] ?? [];

        $control['type'] = 'dropdown-pages';

        return $this->base($id, $label, $configuration = [
            'control' => $control,
            'settings' => $settings,
        ]);
    }

    public function imageUpload(string $id, string $label, array $configuration = [])
    {
        $control = $configuration['control'] ?? [];
        $settings = $configuration['settings'] ?? [];

        return $this->base($id, $label, $configuration = [
            'control' => $control,
            'settings' => $settings,
        ], \WP_Customize_Image_Control::class);
    }

    public function fileUpload(string $id, string $label, array $configuration = [])
    {
        $control = $configuration['control'] ?? [];
        $settings = $configuration['settings'] ?? [];

        return $this->base($id, $label, $configuration = [
            'control' => $control,
            'settings' => $settings,
        ], \WP_Customize_Upload_Control::class);
    }

    public function colorPicker(string $id, string $label, array $configuration = [])
    {
        $control = $configuration['control'] ?? [];
        $settings = $configuration['settings'] ?? [];

        $settings['sanitize_callback'] = 'sanitize_hex_color';

        return $this->base($id, $label, $configuration = [
            'control' => $control,
            'settings' => $settings,
        ], \WP_Customize_Color_Control::class);
    }

    public function base(string $id, string $label, array $configuration = [], $controlClass = null)
    {
        $control = $configuration['control'] ?? [];
        $settings = $configuration['settings'] ?? [];

        $this->wpCustomize->add_setting(
            $this->settingId($id),
            $this->settingArguments($id, $settings)
        );

        if ($controlClass) {
            $this->wpCustomize->add_control(new $controlClass(
                $this->wpCustomize,
                $this->controlId($id),
                $this->controlArguments($id, $label, $control, $exceptKeys = ['type'])
            ));
        } else {
            $this->wpCustomize->add_control(
                $this->controlId($id),
                $this->controlArguments($id, $label, $control)
            );
        }


        return $this;
    }

    protected function settingId($id)
    {
        return "{$this->optionName}[{$id}]";
    }


    protected function controlId($id)
    {
        return "{$this->section}_{$id}";
    }

    /**
     * Get the value of wpCustomize
     */
    public function getWpCustomize()
    {
        return $this->wpCustomize;
    }

    /**
     * Set the value of wpCustomize
     *
     * @return  self
     */
    public function setWpCustomize($wpCustomize)
    {
        $this->wpCustomize = $wpCustomize;

        return $this;
    }

    /**
     * Get the value of optionName
     */
    public function getOptionName()
    {
        return $this->optionName;
    }

    /**
     * Set the value of optionName
     *
     * @return  self
     */
    public function setOptionName($optionName)
    {
        $this->optionName = $optionName;

        return $this;
    }

    /**
     * Get the value of section
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set the value of section
     *
     * @return  self
     */
    public function setSection($section)
    {
        $this->section = $section;

        return $this;
    }

    protected function settingArguments(string $id, array $args = [], array $exceptKeys = [])
    {
        return collect([
            'default' => '',
            'type' => 'option', // option/theme_mod (default theme_mod)
            'capability' => 'edit_theme_options',
            'theme_supports' => [],
            'transport' => 'refresh', // refresh/postMessage (postMessage requires JS)
        ])->merge($args)
        ->filter(function ($value, $key) use ($exceptKeys) {
            return !in_array($key, $exceptKeys);
        })
        ->toArray();
    }

    protected function controlArguments(string $id, string $label, array $args = [], array $exceptKeys = [])
    {
        return collect([
            'label' => $label,
            'description' => '',
            'section' => $this->section,
            'settings' => "{$this->optionName}[{$id}]",
            'priority' => 0, // Optional. themes (0), title_tagline (20), colors (40), header_image (60), background_image (80), static_front_page (120).
            'type' => 'text', //text, checkbox, radio, select, textarea, dropdown-pages, email, url, number, hidden, and date.
            'choices' => [],
            'input_attrs' => [],
        ])->merge($args)
        ->filter(function ($value, $key) use ($exceptKeys) {
            return !in_array($key, $exceptKeys);
        })
        ->toArray();
    }
}
