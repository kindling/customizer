<?php

namespace Kindling\Customizer;

use Kindling\Customizer\Input;
use Illuminate\Contracts\Container\Container as ContainerContract;

class Customizer
{
    protected $wpCustomize;

    public function __construct(\WP_Customize_Manager $wp_customize)
    {
        $this->wpCustomize = $wp_customize;
    }

    public function section(string $section = 'kindling_customizer_section', array $args = [])
    {
        $abstract = "kindling-customizer.{$section}";
        kindling()->singleton($abstract, function (ContainerContract $app) use ($section, $args) {
            // Set the section.
            if (!$this->wpCustomize->get_section($section)) {
                $this->wpCustomize->add_section($section, array_merge([
                    'title' => 'Kindling',
                    'priority' => 120,
                    'description' => '',
                ], $args));
            }

            return new Input($this->wpCustomize, $section);
        });

        return kindling()->make($abstract);
    }
}
