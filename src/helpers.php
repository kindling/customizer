<?php
/**
 * Kindling Customizer - Helpers.
 *
 * @package Kindling_Customizer
 */

function kindling_customizer_options($key = 'kindling_customizer_options', $fresh = false)
{
    static $options;

    if ($options and !$fresh) {
        return $options;
    }

    $options = get_option('kindling_customizer_options', collect([]));

    return $options;
}

function kindling_customizer_option($key, $default = null, $optionKey = 'kindling_customizer_options')
{
    return $options->get($key, $default);
}
