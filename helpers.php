<?php
/**
 * Kindling Customizer - Helpers.
 *
 * @package Kindling_Customizer
 */

function kindling_customizer_options($optionKey = 'kindling_customizer_options', $fresh = false)
{
    static $options;

    if ($options and ! $fresh) {
        return $options;
    }

    $options = get_option($optionKey, []);
    $options = is_array($options) ? $options : [];

    return $options = collect($options);
}

function kindling_customizer_option($id, $default = null, $optionKey = 'kindling_customizer_options')
{
    return kindling_customizer_options()->get($id, $default);
}
